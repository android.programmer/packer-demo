# Network Security
### Physical layer
Wiretapping can be foiled by by enclosing transmissions lines in sealed tubes containing inert gas at high pressure. any attemp to drill into the tube will release some gas, reducing the pressure and triggering an alarm.

### Datalink layer
In this layer, packets on a point to point line can be encrypted as they leave one machine and decryptoed as they enter another. All the details can be handled in the data link layer, with higher layers oblivious to what is going on. This solution breaks down when packets have to traverse multiple routers, however, because packets have to decrypted at each router, leaving them vunlerable to attacks from within router. Also, it does not allow some sessions (e.g. this involving online purchases by credit card) to be protected and others not. Nevertheless, **link encryption** as this method is called, can be added to network and is often useful

### Network layer
In this layer, firewalls can be installed to keep good packets in and bad packets out. IP security also functions in this layer.

### Transport layer
In this layer, entire connection can be encrypted end to end, this is, process to process.

### Application layer
Authentication and nonrepuidation can only be handled in the application layer.


### Two Fundamental Cryptographic Principles
The purpose of these principles are to increase the cost (e.g. computation and time) of the attack

#### Redundancy
#### Freshness