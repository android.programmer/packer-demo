# Packer-demo

This project uses [Vagrant](https://www.vagrantup.com/) to install [Packer](https://www.packer.io/) and build up an Ubuntu 16.04 image on [AWS](https://aws.amazon.com/)
